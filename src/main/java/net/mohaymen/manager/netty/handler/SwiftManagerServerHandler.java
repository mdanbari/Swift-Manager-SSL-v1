/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.mohaymen.manager.netty.handler;

import java.net.InetAddress;

import javax.swing.text.rtf.RTFEditorKit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import net.mohaymen.manager.netty.ChannelRepository;
import net.mohaymen.manager.netty.command.SwiftCommand;
import net.mohaymen.manager.netty.command.SwiftCommandParser;
import net.mohaymen.manager.netty.command.SwiftCommandType;
import net.mohaymen.manager.web.websocket.dto.ActivityDTO;

/**
 * event handler to process receiving messages
 *
 * @author Jibeom Jung
 */
@Component
//@Qualifier("swiftManagerServerHandler")
@ChannelHandler.Sharable
public class SwiftManagerServerHandler extends ChannelInboundHandlerAdapter {
	


    @Autowired
    private ChannelRepository channelRepository;
    
    @Autowired
    SimpMessageSendingOperations messagingTemplate;
    
    @Autowired
    private SwiftCommandParser swiftCommandParser; 
    
   

    private static Logger logger = Logger.getLogger(SwiftManagerServerHandler.class.getName());

//    @Override
//    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//        Assert.notNull(this.channelRepository, "[Assertion failed] - ChannelRepository is required; it must not be null");
//
//        ctx.fireChannelActive();
//        logger.debug(ctx.channel().remoteAddress());
//        String channelKey = ctx.channel().remoteAddress().toString().replaceFirst("/", "").split(":")[0];
//        channelRepository.put(channelKey, ctx.channel());
//    	SwiftCommand command = new SwiftCommand();
//		command.setCommandName("Your channel key is " + channelKey);
//		command.setCommandType(0);
//		command.setArg1("arg1");
//		command.setArg2("arg2");
//		command.setArg3("arg3");			
//		String jsonInString = mapper.writeValueAsString(command);
//
//        ctx.writeAndFlush(jsonInString+"\r\n");
//
//        logger.debug("Binded Channel Count is " + this.channelRepository.size());     
//        
//        
//    }
    
    
    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        // Once session is secured, send a greeting and register the channel to the global channel
        // list so the channel received the messages from others.
        ctx.pipeline().get(SslHandler.class).handshakeFuture().addListener(
                new GenericFutureListener<Future<Channel>>() {
                    @Override
                    public void operationComplete(Future<Channel> future) throws Exception {
                    	String json = swiftCommandParser.endode(SwiftCommandType.PRINT, "\n * Welcome to " + InetAddress.getLocalHost().getHostName() + " secured channel! \n * Your session is protected by " + ctx.pipeline().get(SslHandler.class).engine().getSession().getCipherSuite() + " cipher suite \n * Peer certificates length: "+ctx.pipeline().get(SslHandler.class).engine().getSession().getPeerCertificates().length +"\n");
                        ctx.writeAndFlush(json+"\r\n");
//                        json = swiftCommandParser.endode(SwiftCommandType.PRINT,"Your session is protected by " + ctx.pipeline().get(SslHandler.class).engine().getSession().getCipherSuite() + " cipher suite.");
//                        ctx.writeAndFlush(json+"\r\n");
//                        json = swiftCommandParser.endode(SwiftCommandType.PRINT,"Peer certificates length: "+ctx.pipeline().get(SslHandler.class).engine().getSession().getPeerCertificates().length);                      
//                        ctx.writeAndFlush(json+"\r\n");
                        String channelKey = ctx.channel().remoteAddress().toString().replaceFirst("/", "").split(":")[0];
                        channelRepository.put(channelKey, ctx.channel());
                    }
        });
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String stringMessage = (String) msg;

        logger.debug(stringMessage);

//        String[] splitMessage = stringMessage.split("::");
//
//        if ( splitMessage.length != 2 ) {
//            ctx.channel().writeAndFlush(stringMessage + "\n\r");
//            return;
//        }
//
//        if ( channelRepository.get(splitMessage[0]) != null ) {
//            channelRepository.get(splitMessage[0]).writeAndFlush(splitMessage[1] + "\n\r");
//        }
        
        messagingTemplate.convertAndSend("/topic/agent", stringMessage);
    }
    
    //@SubscribeMapping("/topic/agent")
    @SendTo("/topic/agent")
    public String pushResponse(@Payload String msg)
    {
    	return msg;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(), cause);
        //ctx.close();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx){
        Assert.notNull(this.channelRepository, "[Assertion failed] - ChannelRepository is required; it must not be null");
        Assert.notNull(ctx);

        String channelKey = ctx.channel().remoteAddress().toString();
        this.channelRepository.remove(channelKey);

        logger.debug("Binded Channel Count is " + this.channelRepository.size());
    }

    public void setChannelRepository(ChannelRepository channelRepository) {
        this.channelRepository = channelRepository;
    }
}
