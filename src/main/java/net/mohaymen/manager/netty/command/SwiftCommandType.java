package net.mohaymen.manager.netty.command;

public enum SwiftCommandType {
	PRINT(0), CMD(1);

	private final int cmd;

	public int getCmd() {
		return cmd;
	}

	private SwiftCommandType(int cmd) {
		this.cmd = cmd;
	}

}
