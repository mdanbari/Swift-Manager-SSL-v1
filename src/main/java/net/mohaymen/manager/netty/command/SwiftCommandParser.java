package net.mohaymen.manager.netty.command;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class SwiftCommandParser {

	@Autowired
	private ObjectMapper mapper;

	public String endode(SwiftCommandType commandType, String... args) throws JsonProcessingException {
		SwiftCommand command = new SwiftCommand();
		command.setCommandType(commandType.ordinal());
		command.setCommandName(args[0]);
		if(args.length > 1)
			command.setArg1(args[1]);
		if(args.length > 2)
			command.setArg2(args[2]);
		if(args.length > 3)
			command.setArg3(args[3]);
		String jsonInString = mapper.writeValueAsString(command).replaceAll("\\r\\n", "");
		return jsonInString;
	}

	public SwiftCommand decode(String json) throws IOException {
		SwiftCommand obj = mapper.readValue((String) json, SwiftCommand.class);
		return obj;
	}

}
