(function() {
    'use strict';

    angular
        .module('swiftmanagerApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
